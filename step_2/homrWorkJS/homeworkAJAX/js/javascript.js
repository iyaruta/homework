let jsonObj;
const xr = new XMLHttpRequest();
xr.open('GET', "https://swapi.co/api/films/");
xr.send();
xr.onload = function () {
    if (xr.status != 200) {
        console.log(`${xr.status}, ${xr.statusText}`);
    } else {
        jsonObj = JSON.parse(xr.response);

        const list = document.getElementById('list');
        const olList = document.createElement('ul');

        jsonObj.results.forEach(function (film) {

            const li = document.createElement('li');
            const nameFilm = document.createElement('h3');
            const description = document.createElement('span');
            const button = document.createElement('button');
            const heroes = document.createElement('div');

            heroes.setAttribute("id", "heroes-" + film.episode_id);
            heroes.setAttribute("episode-id", film.episode_id);
            nameFilm.textContent = "Episode - " + film.episode_id + ": " +film.title;
            description.textContent = film.opening_crawl;
            button.textContent = "Список персонажей";

            heroes.appendChild(button);
            li.appendChild(nameFilm);
            li.appendChild(description);
            li.appendChild(heroes);
            olList.appendChild(li);
        });

        list.append(olList);

        document.querySelectorAll('button').forEach(function (el) {
            el.addEventListener("click", function () {
                let id = Number(this.parentElement.getAttribute('episode-id'));
                findCharacters(id);
            });
        });

    }

};

function findCharacters(episodeId) {
    jsonObj.results.forEach(function (film) {
        if (film.episode_id === episodeId) {
            const heroList = document.createElement('ul');
            const heroesDiv = document.getElementById("heroes-" + episodeId);
            while (heroesDiv.firstChild) {
                heroesDiv.firstChild.remove();
            }
            heroesDiv.appendChild(heroList);
            film.characters.forEach(function (heroUrl) {
                const xr = new XMLHttpRequest();
                xr.open('GET', heroUrl);
                xr.send();
                xr.onload = function () {
                    if (xr.status != 200) {
                        console.log(`${xr.status}, ${xr.statusText}`);
                    } else {
                        const hero = JSON.parse(xr.response);
                        const heroLi = document.createElement('li');
                        heroLi.textContent = hero.name;
                        heroList.appendChild(heroLi);
                    }
                }

            })
        }

    })
}