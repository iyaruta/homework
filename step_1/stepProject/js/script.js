// Services scripts

$(function () {
    $("#tabs").tabs();
});

// Portfolio scripts

$('.portfolio .services-tabs').click(function () {
    let activeTab = $(this).attr('ui-pf');
    let activeGroup = $('.ui-show-more').attr('ui-pf');
    if (activeGroup != activeTab) {
        $("[class^='ui-pf-'],[class*=' ui-pf-']").hide();
        $('.ui-show-more').attr('ui-pf', activeTab);
        applyOnGroup(activeTab);
    }
});

$('.ui-show-more').click(function () {
    let activeGroup = $(this).attr('ui-pf');
    applyOnGroup(activeGroup)

});

function applyOnGroup(group) {
    if (group == 'all') {
        showMore($("[class^='ui-pf-'],[class*=' ui-pf-']"));
    } else {

        let cssGroup = '.ui-pf-' + group;
        let subElements = $(cssGroup);
        showMore(subElements)
    }
}

function showMore(allElements) {
    let onlyHidden = allElements.filter(function () {
        return $(this).is(":hidden");
    });

    let onlyTwoHiddenElements = onlyHidden.filter(function (index) {
        return index < 12;
    });

    onlyTwoHiddenElements.show();

    showMoreButton(allElements);
}

function showMoreButton(allElements) {
    let hidden = allElements.filter(function () {
        return $(this).is(":hidden");
    }).size();
    if (hidden == 0) {
        $('.ui-show-more').hide();
    } else {
        $('.ui-show-more').show();
    }
}

$(function () {
    $("#portfolio-tabs").tabs();
});

applyOnGroup('all');

// Our Team
$(function () {
    $("#team-tabs").tabs();
});


$('.tabs-control').click(function () {
    let direction = $(this).hasClass("ui-control-left") ? -1 : 1;
    let tabs = $(".tabs-photo .photo-tab");

    let selected = $(".tabs-photo").index($(".tabs-photo.ui-state-active"));
    let next = selected + direction;
    if (next < 0) {
        next = tabs.size() - 1;
    } else if (next > tabs.size() - 1) {
        next = 0;
    }
    tabs.get(next).click();
});
