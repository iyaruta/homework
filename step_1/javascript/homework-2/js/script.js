let firstNumber = getNumber(1);
let lastNumber = getNumber(firstNumber);

function getNumber(minValue) {
    let value = Number(prompt(`Enter number more than ${minValue}`, ""));
    while (isInteger(value) === false || value <= minValue){
        value = Number(prompt(`Sorry, your number is not correct or less than ${minValue}. Please enter correct number`, value));
    }
    return value;
}

function isInteger(num) {
    return (num ^ 0) === num;
}

function getPrimes(maxValue) {
    let arr = new Array(maxValue + 1);

    for (let i = 2; i < arr.length; i++) {
        if (arr[i] === undefined) {
            for (let j = i * 2; j <= maxValue; j += i) {
                arr[j] = true;
            }
        }
    }
    return arr;
}

let result = getPrimes(lastNumber);
for (let i = firstNumber; i <= lastNumber; i++) {
    if (result[i] === undefined) {
        console.log(i)
    }
}
