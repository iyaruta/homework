
function factorial (num) {
    /////////////////// version 1 ///////////////
    // var  a = 1;
    // for(let i = 1; i <= num; i++) {
    //     a = a * i;
    // }

    ////////////// version 2 //////////////
    if(num === 0 || num === 1) {
        return 1;
    }
    return factorial(num - 1) * num;
};

let number = Number(prompt("Enter your number", ""));

alert(`Factorial your number: ${factorial(number)}`);