function createNewUser(fName, lName) {
    user = {};

    Object.defineProperty(user, "firstName", {
        set: (val) => {
            this.firstName = val;
        },
        get: () => {
            return this.firstName
        },
    });

    Object.defineProperty(user, "lastName", {
        set: (val) => {
            this.lastName = val;
        },
        get: () => {
            return this.lastName
        },
    });

    Object.defineProperty(user, "login", {
        get: () => {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.slice(0).toLowerCase()
        },
    });
    user.firstName = fName;
    user.lastName = lName;
    return user;
}


let firstName = prompt(`Write your first name`, "");
let lastName = prompt(`Write your last name, please`, "");

alert(`Your login: ` + createNewUser(firstName, lastName).login);




//////////////////////// VERSION 2 ///////////////

function createNewUser2(firstName, lastName) {
    user = {};

    Object.defineProperties(user, {
        firstName: {
            set: (val) => {
                this.firstName = val;
            },
            get: () => {
                return this.firstName
            },

        },

        lastName: {
            set: (val) => {
                this.lastName = val;
            },
            get: () => {
                return this.lastName
            },
        },

        login: {
            get: () => {
                return firstName.charAt(0).toLowerCase() + lastName.slice(0).toLowerCase()
            },
        }
    });
    return user;
}

// let firstN = prompt(`Write your name`, "");
// let lastN = prompt(`Write your first name, please`, "");
//
// alert(`Your login: ` + createNewUser2(firstN, lastN).login);



