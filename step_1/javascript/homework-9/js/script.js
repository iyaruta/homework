function countAge(birthDay) {
    let day = birthDay.slice(0, 2);
    let month = birthDay.slice(3, 5) - 1;
    let year = birthDay.slice(6, 11);

    let toDate = new Date();

    let age = toDate.getFullYear() - year;
    if (toDate.getMonth() < month || (toDate.getMonth() == month && toDate.getDate() < day)) {
        age--;
    }

    let sign = signs(month, day);
    let chineseSign = getChineseSign(year);

    return {
        "age": age,
        "sign": sign,
        "chineseSign": chineseSign
    }
}

function signs(month, date) {
    let value = '';
    if (month == 0 && date >= 20 || month == 1 && date <= 18) {
        value = "Aquarius";
    } else if (month == 1 && date >= 19 || month == 2 && date <= 20) {
        value = "Pisces";
    } else if (month == 2 && date >= 21 || month == 3 && date <= 19) {
        value = "Aries";
    } else if (month == 3 && date >= 20 || month == 4 && date <= 20) {
        value = "Taurus";
    } else if (month == 4 && date >= 21 || month == 5 && date <= 21) {
        value = "Gemini";
    } else if (month == 5 && date >= 22 || month == 6 && date <= 22) {
        value = "Cancer";
    } else if (month == 6 && date >= 23 || month == 7 && date <= 22) {
        value = "Leo";
    } else if (month == 7 && date >= 23 || month == 8 && date <= 22) {
        value = "Virgo";
    } else if (month == 8 && date >= 23 || month == 9 && date <= 22) {
        value = "Libra";
    } else if (month == 9 && date >= 23 || month == 10 && date <= 21) {
        value = "Scorpio";
    } else if (month == 10 && date >= 22 || month == 11 && date <= 21) {
        value = "Sagittarius";
    } else if (month == 11 && date >= 22 || month == 0 && date <= 19) {
        value = "Capricorn";
    } else {
        value = "Invalid Date";
    }

    return value;
}

function getChineseSign(birthyear) {
    let x = (1901 - birthyear) % 12;
    let value = '';
    if (x == 1 || x == -11) {
        value = "Rat";
    }
    if (x == 0) {
        value = "Ox";
    }
    if (x == 11 || x == -1) {
        value = "Tiger";
    }
    if (x == 10 || x == -2) {
        value = "Rabbit/Cat";
    }
    if (x == 9 || x == -3) {
        value = "Dragon";
    }
    if (x == 8 || x == -4) {
        value = "Snake";
    }
    if (x == 7 || x == -5) {
        value = "Horse";
    }
    if (x == 6 || x == -6) {
        value = "Sheep";
    }
    if (x == 5 || x == -7) {
        value = "Monkey";
    }
    if (x == 4 || x == -8) {
        value = "Cock/Phoenix";
    }
    if (x == 3 || x == -9) {
        value = "Dog";
    }
    if (x == 2 || x == -10) {
        value = "Boar";
    }
    return value;
}

let birthday = prompt(`Is our birthday?`, "08.04.1987");

let profile = countAge(birthday);

alert(`Ваш возрост ${profile.age} и знак зодиака ${profile.sign} / ${profile.chineseSign}`);
