const user = [
    {
        name: "Ivan",
        surname: "Ivanov", //// Ivan
        gender: "male",
        age: 30
    },
    {
        name: "Anna",
        surname: "Ivanova",
        gender: "female",
        age: 22
    },
    {
        name: "Vita",
        surname: "Sergevuch",
        gender: "male",
        age: 30
    },
    {
        name: "Andrey",
        surname: "Anatolivna",
        gender: "female",
        age: 22
    }];

const listUser = [
    {
        name: "Anna",
        surname: "Ivanova",
        gender: "female",
        age: 22
    },
    {
        name: "Andrey",
        surname: "Sergevuch",
        gender: "male",
        age: 30
    },
    {
        name: "Vita",
        surname: "Victorivna",
        gender: "female",
        age: 22
    }];

function excludeBy(peopleList, excluded, propertyName) {
    let excludedMap = excluded.map(function (x) {
        return x[propertyName];
    });

    return peopleList.filter(function (obj) {
        return !excludedMap.includes(obj[propertyName]);
    });
}

let a = excludeBy(user, listUser, "name");
for (let i = 0; i <= a.length; i++) {
    console.log(a[i]);
}


