let testObject = {
    num1: 1,
    str2: "string",
    str3: "css",
    str4: "",
    obj5: {
        first: "winter",
        second: "autumn",
        jbj: {
            first2: "summer",
            second2: "April",
            last: {
                last3: "",
                finish3: "finishCopy",
            },
            number: 144,
            bus: 118,
            numberString: "4",
        },

        num6: 6,
        width: 300,
        height: 200,
        title: "Menu",
        arr5: [1, 4, 5]
    },
    array: [1, 4, 5, "s", {}]
};


function copy(obj) {
    let ourNewObject = {};
    for (let key in obj) {
        if (obj[key] instanceof Object) {
            ourNewObject[key] = copy(obj[key]);
        } else if (obj[key] instanceof Array) {
            ourNewObject[key] = obj[key].slice(0, obj[key].length);
        } else {
            ourNewObject[key] = obj[key];
        }

    }
    return ourNewObject;
}

let cop = copy(testObject);
for (let key in cop) {
    console.log(key) + " " + console.log(cop[key])
}
