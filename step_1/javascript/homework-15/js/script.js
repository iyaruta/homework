let startTime;
let pauseTime = 0;
let timerId;

function toTime() {
    let now = new Date(new Date() - startTime);
    // let now = new Date(new Date() - startTime);
    let minutes = now.getMinutes();
    minutes = minutes < 10 ? "0" + minutes : minutes;
    let seconds = now.getSeconds();
    seconds = seconds < 10 ? "0" + seconds : seconds;
    let milliseconds = now.getMilliseconds();
    milliseconds = milliseconds < 100 ? (milliseconds < 10 ? "00" + milliseconds : "0" + milliseconds) : milliseconds;
    return minutes + ":" + seconds + ":" + milliseconds;
}

$("#start").click(function () {
    startTime = new Date();
    timerId = setInterval(function () {
        $("#display").text(toTime());
    }, 1);

    $(this).hide();
    $("#pause").show();

});

$("#pause").click(function () {
    if (pauseTime === 0) {
        clearInterval(timerId);
        pauseTime = new Date();
    } else {
        startTime = new Date(startTime.getTime() + (new Date().getTime() - pauseTime));
        timerId = setInterval(function () {
            $("#display").text(toTime());
        }, 1);
        pauseTime = 0;
    }
});

$("#clear").click(function () {
    clearInterval(timerId);
    startTime = 0;
    pauseTime = 0;
    $("#pause").hide();
    $("#start").show();
    $("#display").text("00:00:000");

});
