let name = "";
let age = "";

while (!name.trim()) {
    name = (prompt("enter your name", name));
    if (!name.trim()) {
        alert("name is empty");
    }
}

while (isNaN(age) || age <= 0) {
    age = prompt("enter your age", age);
    if (isNaN(age) || age <= 0) {
        alert("invalid age");
    } else {
        age = Number(age);
    }
}

if (age < 18) {
    alert("You are not allowed to visit this website");
} else if (age <= 22) {
    if (confirm("Are you sure you want to continue?")) {
        alert(`Welcome ${name}`)
    } else {
        alert("You are not allowed to visit this website.");
    }
} else {
    alert(`Welcome ${name}`);
}