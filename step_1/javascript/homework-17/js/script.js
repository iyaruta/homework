    $("#click").click(function () {
        let inputSize = $('<input>');
        inputSize.attr("id", "diameter");
        inputSize.attr("type", "number");
        inputSize.attr("placeholder", "diameter");
        $("#container").append(inputSize);

        let inputColor = $('<input>');
        inputColor.attr("id", "color");
        inputColor.attr("type", "color");
        $("#container").append(inputColor);

        let inputSubmit = $('<input>');
        inputSubmit.attr("id", "btn");
        inputSubmit.attr("type", "submit");
        inputSubmit.attr("value", "create circle");
        $("#container").append(inputSubmit);


        inputSubmit.click(function () {
            let d = $("#diameter").val();
            let color =$("#color").val();
            drawCircle(d, color);
        });

        $("#container").append(inputSubmit);
        $("#click").remove();
    });

function drawCircle(d, color) {
    let circle = $('<div></div>');
    circle.attr("id", "circle");
    circle.width(d);
    circle.height(d);
    circle.css("background-color", color);
    circle.css("border-radius", "50%");
    $("#container").append(circle);
}