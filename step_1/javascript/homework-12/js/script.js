document.getElementById("click").onclick = function() {

    let inputSize = document.createElement('input');
    inputSize.setAttribute("id", "diameter");
    inputSize.setAttribute("type", "number");
    inputSize.setAttribute("placeholder", "diameter");
    document.getElementById("container").appendChild(inputSize);

    let inputColor = document.createElement('input');
    inputColor.setAttribute("id", "color");
    inputColor.setAttribute("type", "color");
    document.getElementById("container").appendChild(inputColor);

    let inputSubmit = document.createElement('input');
    inputSubmit.setAttribute("id", "btn");
    inputSubmit.setAttribute("type", "submit");
    inputSubmit.setAttribute("value", "create circle");
    inputSubmit.onclick = function() {
        let d = document.getElementById("diameter").value;
        let color = document.getElementById("color").value;
        drawCircle(d, color);
    };
    document.getElementById("container").appendChild(inputSubmit);

    let inputButton = document.getElementById("click");
    inputButton.parentNode.removeChild(inputButton);

};

function drawCircle(d, color) {
    let circle = document.createElement('div');
    circle.setAttribute("id", "circle");
    circle.style.width = d + "px";
    circle.style.height = d + "px";
    circle.style.backgroundColor = color;
    circle.style.borderRadius = "50%";
    document.getElementById("container").appendChild(circle);
}